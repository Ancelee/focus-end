/**
 * focusEnd
 * 光标移到末
 * 尾调用办法：$(element).focusEnd();
 * http://www.ancelee.com
 */
;(function($) {
	$.fn.focusEnd = function() {
		var $Obj = this;
		var fun = {
			setCursorPosition: function(position) {
				if ($Obj.lengh == 0) return this;
				return this.setSelection(position, position);
			},
			setSelection: function(selectionStart, selectionEnd) {
				if ($Obj.lengh == 0) return $Obj;
				input = $Obj[0];

				if (input.createTextRange) {
					var range = input.createTextRange();
					range.collapse(true);
					range.moveEnd('character', selectionEnd);
					range.moveStart('character', selectionStart);
					range.select();
				} else if (input.setSelectionRange) {
					input.focus();
					input.setSelectionRange(selectionStart, selectionEnd);
				}
				return $Obj;
			}
		};		
		fun.setCursorPosition(this.val().length);
	};
})(jQuery);